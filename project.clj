(defproject clj-ticket "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.5.1"]
                 [ring/ring-defaults "0.2.1"]
                 [ring/ring-json "0.4.0"]
                 [org.clojure/data.json "0.2.6"]
                 [clj-http "3.7.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [korma "0.4.3"]
                 [mysql/mysql-connector-java "5.1.45"]
                 [org.clojure/core.async "0.4.474"]
                 [com.taoensso/carmine "2.17.0"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler clj-ticket.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
