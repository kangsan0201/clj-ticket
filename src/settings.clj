(ns settings)

(def service-name "ticket")

(def addr "http://localhost")

(def hosts {:user     {:addr addr :port 8081}
            :offering {:addr addr :port 8084}
            :wallet   {:addr addr :port 8082}
            :risk     {:addr addr :port 8098}
            :obs      {:addr addr :port 8086}})

(def db-conf {:dbtype   "mysql"
              :host     "localhost"
              :user     "kwiffuser"
              :password "kwiffpassword"
              :dbname   "kwiff_ticket"})

(def redis-conf {:host "localhost"
                 :port 6379})