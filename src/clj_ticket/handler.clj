(ns clj-ticket.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [ring.middleware.json :as middleware]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [services.user :as user-service]
            [services.event :as event-service]
            [services.risk :as risk-service]
            [services.ticket :as ticket-service]
            [utilities.serializer :as serializer]
            [workers.accept :as accept-worker]))

(defn ok [body]
  {:status 200 :body (assoc body :success true)})

(defn nok [body]
  {:status 200 :body (assoc body :success false)})

(defn create-ticket [user-msg]
  (let [user-id (:userId user-msg)
        stake (:amount user-msg)
        bets (:bets user-msg)
        acca-odds (get-in user-msg [:accumulator :odds])
        user-verified (user-service/verify-user user-id stake)
        events (event-service/get-events bets)
        no-event-suspended (event-service/check-event-suspended events)
        odds-correct (event-service/check-odds-changed bets events)
        acca-odds-correct (event-service/check-acca-odds acca-odds bets)
        risk-verified (risk-service/accept-reject user-id stake bets events)
        accept-delay (event-service/get-pending-timer events)]
    (do
      (println user-id stake user-verified 
               "\n--> event result:" (:code events) 
               "\n--> messages:" (:message events)
               "\n--> accept-delay" accept-delay)
      (let [ticket-id (ticket-service/create-ticket user-msg events accept-delay)
            wait-ticket (ticket-service/get-accept-wait-ticket ticket-id)
            serialized-ticket (serializer/ticket wait-ticket)]
        (accept-worker/register-wait-timer ticket-id accept-delay user-msg)
        (ok serialized-ticket)))))
  
(defn undo-ticket [user-msg]
  (let [ticket-id (:ticketId user-msg)
        success (accept-worker/reject-ticket-by-user ticket-id)]
    (if success
      (ok {})
      (nok {}))))

(defn get-ticket [user-msg]
  (let [ticket-id (:ticketId user-msg)]
    (-> (ticket-service/get-accepted-ticket ticket-id)
        (serializer/ticket)
        (ok))))
        
(defn list-ticket [user-msg]
  (let [{:keys [:userId :active :limit :offset :groupBy]} user-msg]
    (->> (ticket-service/get-list userId active limit offset groupBy)
         (map #(serializer/ticket %))
         (hash-map :tickets)
         (ok))))

(def handlers {:ticketcreate create-ticket
               :ticketundo undo-ticket
               :ticketget get-ticket
               :ticketlist list-ticket})
            
(defroutes app-routes
  (POST "/reqrep/:category/:subject" request
    (let [data (get request :body)
          {category :category subject :subject} (:params request)
          command (str category subject)]
      (try
        @(future ((get handlers (keyword command)) data))
        (catch Exception e
          (do 
            (let [ed (ex-data (.getCause e))
                  code (:code ed nil)
                  message (:message ed nil)
                  data (:data ed nil)]
              (if (nil? code)
                (do
                  (.printStackTrace e)
                  (nok {:code "UNEXPECTED_ERROR"}))
                (do
                  (println "Error caught, detail:" ed)
                  (let [resp {:code code}
                        assoc-key #(if (empty? %3) %1 (assoc %1 %2 %3))]
                    (-> resp 
                        (assoc-key :message message)
                        (assoc-key :data data)
                        (nok))))))))
        (finally 
          (println "API" command "done")))))

  (route/not-found "Not Found"))
 
(def app
  (-> (handler/site app-routes)
      (middleware/wrap-json-body {:keywords? true})
      middleware/wrap-json-response))
  


; var single = {"userId":3,"ip":"192.168.0.1","amount":0.1,"accumulator":false,"bets":[{"sportId":1,"eventId":1,"offerId":1,"odds":2.05,"fractional":"4/5","outcomeId":11}],"coord":{"latitude":51.49589967008448,"longitude":-0.23071825504303}}
; send('clojure', 'ticket:create', single)
; var acca = {"userId":3,"ip":"192.168.0.1","amount":0.1,"accumulator":{"odds":2.77,"fractional":""},"bets":[{"sportId":1,"eventId":1,"offerId":1,"odds":2.05,"outcomeId":11},{"sportId":2,"eventId":43601,"offerId":1,"odds":1.35,"outcomeId":11}],"coord":{"latitude":51.49589967008448,"longitude":-0.23071825504303}} 
; send('clojure', 'ticket:create', acca)
;; acca
; send('clojure', 'ticket:create', {"userId":3,"ip":"192.168.0.1","amount":0.1,"accumulator":{"odds":4.75,"fractional":""},"bets":[{"sportId":1,"eventId":1,"offerId":1,"odds":1.8,"outcomeId":11},{"sportId":1,"eventId":43597,"offerId":1,"odds":2.95,"outcomeId":11}],"coord":{"latitude":51.49589967008448,"longitude":-0.23071825504303}})
;; acca odds changed 
; send('clojure', 'ticket:create', {"userId":3,"ip":"192.168.0.1","amount":0.1,"accumulator":{"odds":4.75,"fractional":""},"bets":[{"sportId":1,"eventId":1,"offerId":1,"odds":2.8,"outcomeId":11},{"sportId":1,"eventId":43597,"offerId":1,"odds":2.95,"outcomeId":11}],"coord":{"latitude":51.49589967008448,"longitude":-0.23071825504303}})
;; acca invalid acca odds
; send('clojure', 'ticket:create', {"userId":3,"ip":"192.168.0.1","amount":0.1,"accumulator":{"odds":4.95,"fractional":""},"bets":[{"sportId":1,"eventId":1,"offerId":1,"odds":1.8,"outcomeId":11},{"sportId":1,"eventId":43597,"offerId":1,"odds":2.95,"outcomeId":11}],"coord":{"latitude":51.49589967008448,"longitude":-0.23071825504303}})
; {
;   "userId": 3,
;   "ip": "192.168.0.1",
;   "amount": 0.1,
;   "accumulator": false,
;   "bets": [{
;     "sportId": 1,
;     "eventId": 1,
;     "offerId": 1,
;     "odds": 1.8,
;     "fractional": "4/5",
;     "outcomeId": 11
;   }],
;   "coord": {
;     "latitude": 51.49589967008448,
;     "longitude": -0.23071825504303
;   }
; }
; send('clojure', 'ticket:list', {groupBy:false, userId: 3, offset:0, limit:10})
; send('ticket', 'ticket:list', {groupBy:false, userId: 3, offset:0, limit:10})
; send('clojure', 'ticket:list', {groupBy: "event", userId: 3, offset:0, limit:10})

;; GET
; send('clojure', 'ticket:get', {sportId: 0, ticketId: 105097})