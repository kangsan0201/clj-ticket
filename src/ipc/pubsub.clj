(ns ipc.pubsub
  (:require [settings :refer [redis-conf]]
            [taoensso.carmine :as car :refer (wcar)]))

(def server-uri (str "redis://" 
                     (:host redis-conf) 
                     ":" 
                     (:port redis-conf)))

(def server-conn {:pool {} :spec {:uri server-uri}})
(defmacro wcar* [& body] `(car/wcar {:pool {} :spec {}} ~@body))

; (defn publish [] ())

(defn register
  "Register callback and return listener"
  [cmd cb]
  (car/with-new-pubsub-listener
    (:spec server-conn)
    {cmd (fn callback [msg] ;; msg format => [message event:update event-object]
            (let [msg-type (nth msg 0)
                  msg-name (nth msg 1)
                  msg-body (nth msg 2)]
              (if (and (= msg-type "message")
                       (= msg-name cmd))
                (cb msg-body))))}
    (car/subscribe cmd)))

(defn deregister [listener]
  (println "deregister listener done")
  (car/close-listener listener))