(ns ipc.reqrep
  (:require [clojure.data.json :as json]
            [clj-http.client :as client]
            [clojure.string :as string]
            [settings :as settings]
            [clojure.walk :as walk]))

(def get-host-addr settings/addr)

(defn get-host-info [target]
  (let [target-key (keyword target)
        info (target-key settings/hosts {})
        addr (:addr info)
        port (:port info)]
    info))

(defn get-url [target command]
  (let [host (get-host-info target)
        resource (string/replace command #":" "/")]
  (format "%s:%s/reqrep/%s" (:addr host) (:port host) resource)))

(defn get-payload [map-type-data]
  {:body (json/write-str map-type-data)
    :content-type :json
    :accept :json})
    
(defn req-post [url payload]
  (client/post url payload))

(defn get-resp-body [resp] 
  (walk/keywordize-keys (json/read-str (:body resp))))

(defn send [target command data]
  (let [url     (get-url target command)
        payload (get-payload data)
        resp    (req-post url payload)]
    (get-resp-body resp)))
