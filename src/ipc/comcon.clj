(ns ipc.comcon
  (:require [settings :refer [service-name redis-conf]]
            [clojure.data.json :as json]
            [clojure.walk :as walk]
            [taoensso.carmine :as car :refer (wcar)]))
; Use Redis list feature and use lpush to push a message and brpop to pop a message.
; Example of redis list key: comcon/ticket/event:update

;; Because of the type class "java.sql.Timestamp" 
(extend-type java.sql.Timestamp
  json/JSONWriter
  (-write [date out]
  (json/-write (str date) out)))

(def server-uri (str "redis://" 
                     (:host redis-conf) 
                     ":" 
                     (:port redis-conf)))

(def server-conn {:pool {} :spec {:uri server-uri}})
(defmacro wcar* [& body] `(car/wcar {:pool {} :spec {}} ~@body))

(defn get-list-key [svc cmd]
  (str "comcon/" svc "/" cmd))

(defn push [services cmd data]
  (let [target-services (if (string? services) 
                          [services] 
                          services)]
    (doseq [svc target-services]
      (let [list-key (get-list-key svc cmd)
            stringified (json/write-str data)]
        (println "comcon push list-key" list-key)
        (wcar* (car/lpush list-key stringified))))))

(def brpop-timeout 20)

(defn pop [cmd cb]
  (let [list-key (get-list-key service-name cmd)]
    (-> (wcar* (car/brpop list-key brpop-timeout))
        (json/read-str)
        (walk/keywordize-keys))))