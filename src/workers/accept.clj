(ns workers.accept
  (:require [services.ticket :as ticket-service]
            [services.risk :as risk-service]
            [services.event :as event-service]
            [services.wallet :as wallet-service]
            [services.user :as user-service]
            [services.obs :as obs-service]
            [utilities.serializer :as serializer]
            [clojure.core.async :as async 
              :refer [>! <! >!! <!! go chan buffer thread close! alts!!]]
            [ipc.pubsub :as pubsub]
            [ipc.comcon :as comcon]
            [db.static :refer [rejection-reason]]))

(def ticket-state-map (atom {}))

(defn get-ticket-id-key [ticket-id]
  (keyword (str ticket-id)))

(defn find-ticket-state [ticket-id]
  (let [ticket-id-key (get-ticket-id-key ticket-id)]
    (ticket-id-key @ticket-state-map)))

(defn add-ticket-state [ticket-id]
  (let [ticket-id-key (get-ticket-id-key ticket-id)]
    (swap! ticket-state-map assoc ticket-id-key "pending")))

(defn change-ticket-state
  "Return true as state changes successfully otherwise return false"
  [ticket-id new-state]
  (do (println "Trying to change-ticket-state" ticket-id "to" new-state))
  (let [ticket-id-key (get-ticket-id-key ticket-id)
        oldval (assoc @ticket-state-map ticket-id-key "pending")
        newval (assoc @ticket-state-map ticket-id-key new-state)]
    (compare-and-set! ticket-state-map oldval newval)))

(defn del-ticket-state [ticket-id]
  (let [ticket-id-key (get-ticket-id-key ticket-id)]
    (swap! ticket-state-map dissoc ticket-id-key)))

(defn find-bet-on-event [bets event-id]
  (->> bets 
       (filter #(= (:eventId %) event-id))
       (first)))

(defn event-suspended? 
  "Return true as an event suspended otherwise return false"
  [event] 
  (pos? (:suspended event)))

(defn odds-changed? 
  "Return true as it does changed otherwise return false"
  [bet event]
  (let [found-outcome (event-service/find-matching-outcome bet event)]
    (nil? found-outcome)))

(defn reject-ticket-by-system [ticket-id user-msg pub-event]
  (try
    ; (throw ;; FIXME - Test only
    ;   (ex-info "Event suspended"
    ;     {:code "EVENT_SUSPENDED"}))  
    (let [{:keys [:userId :bets]} user-msg
          bet-on-event (find-bet-on-event bets (:id pub-event))
          odds-check-required (user-service/check-user-odds-validation (:userId user-msg))]
      (if (some? bet-on-event)
        (do
          (event-service/check-event-suspended [pub-event])
          (if odds-check-required 
            (event-service/check-odds-changed bets [pub-event])))))
    (catch Exception e
      (let [ed (ex-data e) code (:code ed)]
        (if (some? code)
          (let [ticket-state-rejected? (change-ticket-state ticket-id "rejected")
                rejected-by-system (:by-system rejection-reason)]
            (if ticket-state-rejected?
              (do
                (ticket-service/reject-ticket ticket-id rejected-by-system)
                (let [rejected-ticket (ticket-service/get-rejected-ticket ticket-id)
                      serialized-ticket (serializer/ticket rejected-ticket)
                      payload {:ticket serialized-ticket :reason code}]
                  (println "Push ticket:rejected message" payload)
                  (comcon/push ["front"] "ticket:rejected" payload)))))
          (println "reject-ticket-by-system() Error caught:" e))))))

(defn register-listener [ticket-id user-msg]
  (let [cmd "event:update"
        cb (partial reject-ticket-by-system ticket-id user-msg)]
    (pubsub/register cmd cb)))

(defn accept-or-reject [data]
  (let [{:keys [:ticket-id :delay :user-msg]} data
        listener (register-listener ticket-id user-msg)
        odds-check-required (user-service/check-user-odds-validation (:userId user-msg))]
    (try
      (Thread/sleep (* 1000 delay))
      (if (false? (change-ticket-state ticket-id "processing"))
        (throw
          (ex-info "Unable to accept ticket because ticket is already rejected"
            {:code "PENDING_TICKET_REJECTED"
             :data {:ticket-id ticket-id
                    :user-msg user-msg}})))
      (let [bets (:bets user-msg)
            acca-odds (get-in user-msg [:accumulator :odds])
            user-id (:userId user-msg)
            coord (:coord user-msg)
            ticket (ticket-service/get-accept-wait-ticket ticket-id)
            events (event-service/get-events bets)
            no-event-suspended (event-service/check-event-suspended events)
            odds-correct (if odds-check-required (event-service/check-odds-changed bets events) true)
            risk (risk-service/risk-entry ticket events)
            boost (obs-service/get-boost ticket coord)
            wallet (wallet-service/pay-ticket ticket)
            accepted-ticket (ticket-service/accept-n-get-ticket ticket boost)
            serialized-ticket (serializer/ticket accepted-ticket)]
        (comcon/push ["inspector" "front"] "ticket:accepted" {:ticket serialized-ticket}))
      (catch Exception e
        (let [code (:code (ex-data e))]
          (println "Error caught:" e)
          (println "Data:" data)
          (if (and (some? code) (not= "PENDING_TICKET_REJECTED"))
            (do
              (ticket-service/reject-ticket ticket-id (:by-system rejection-reason))
              (let [rejected-ticket (ticket-service/get-rejected-ticket ticket-id)
                    serialized-ticket (serializer/ticket rejected-ticket)
                    payload {:ticket serialized-ticket :reason code}]
                (println "Push ticket:rejected message" payload)
                (comcon/push ["front"] "ticket:rejected" payload))))))
      (finally
        (del-ticket-state ticket-id)
        (pubsub/deregister listener)
        (println "accept-or-reject() done")))))

(defn ticket-accept-machine []
  (let [in (chan)]
    (go (let [data (<! in)]
      (accept-or-reject data)))
    in))

(defn register-wait-timer [ticket-id delay user-msg]
  (let [in (ticket-accept-machine)]
    (add-ticket-state ticket-id)
    (async/put! in {:ticket-id ticket-id
                    :delay delay
                    :user-msg user-msg})))

(defn reject-ticket-by-user [ticket-id]
  (let [ticket-state-rejected? (change-ticket-state ticket-id "rejected")
        rejected-by-user (:by-user rejection-reason)]
    (if ticket-state-rejected?
      (if-let [result (ticket-service/reject-ticket ticket-id rejected-by-user)]
        (do
          (comcon/push ["insight"] "ticket:rejected" {:ticketId ticket-id})
          (println "reject-ticket-by-user() done" "ticket-id" ticket-id "result" result)
          true))
      (throw
        (ex-info "Ticket reject by user failed"
          {:code "REJECT_TICKET_FAILED"})))))


; (println "---> ticket-state-map: " @ticket-state-map)
; (println "---> ticket-id: " ticket-id)
; (println "---> delay: " delay)
; (println "---> user-msg: " user-msg)
; (println "---> acca-odds: " acca-odds)
; (println "---> boost: " boost)
; (println "---> wallet: " wallet)
; (clojure.pprint/pprint accepted-ticket)