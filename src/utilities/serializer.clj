(ns utilities.serializer
  (:require [db.static :as static]
            [clojure.string :as str]))

(defn serialize-ticket-status [to_be_accepted_at rejected_at]
  (cond 
    (and (nil? to_be_accepted_at) (nil? rejected_at)) "pending"
    (and (some? to_be_accepted_at) (nil? rejected_at)) "accepted"
    (and (some? to_be_accepted_at) (some? rejected_at)) "rejected"
    :else "unknown"))

(defn serialize-ticket-result [result settled_at payout]
  (if (some? result) 
    {:finalStatus result
     :settledAt settled_at
     :profit payout}
    nil))

(defn serialize-ticket-accumulator [ticket]
  (let [{:keys [:bet_count :odds :fractional :balanced_odds :balanced_fractional :balance_cost :multiplier]} ticket]
    (if (> bet_count 1) {:odds odds
                         :fractional fractional 
                         :balancedOdds balanced_odds
                         :balancedFractional balanced_fractional
                         :balanceCost balance_cost
                         :multiplier multiplier}
                        nil)))

(defn serialize-football [details]
  (-> {}
      (into {:competition {:id (:competition_id details)
                           :name (:competition_name details)}})
      (into {:homeTeam {:name (:home_team_name details) 
                        :short (:home_team_short_name details)}})
      (into {:awayTeam {:name (:away_team_name details) 
                        :short (:away_team_short_name details)}})
      (into {:scoreAtTimeOfBet {:home (:event_current_score_home details) 
                                :away (:event_current_score_away details)}})
      (into {:halfScore {:home (:event_half_score_home details) 
                        :away (:event_half_score_away details)}})
      (into {:finalScore {:home (:event_final_score_home details) 
                          :away (:event_final_score_away details)}})
      (into {:extraTimeScore {:home (:event_et_score_home details) 
                              :away (:event_et_score_away details)}})
      (into {:penaltyScore {:home (:event_pen_score_home details) 
                            :away (:event_pen_score_away details)}})
      (into {:matchTime {:minutes (:matchtime_min details) 
                         :seconds (:matchtime_sec details)
                         :extraMinutes (:matchtime_extra_min details) 
                         :extraSeconds (:matchtime_extra_sec details)}})))

(defn serialize-tennis [details] 
  (let [{:keys [:set_score_home :set_score_away :game_score_home :game_score_away]} details
        has-final-score (true? (some #(pos? %) (filter some? [set_score_home set_score_away game_score_home game_score_away])))
        base {:numberOfSets (:number_of_sets details)
              :competition {:id (:competition_id details)
                            :name (:competition_name details)}
              :homeParticipants {:name (:home_participant_name details) 
                                 :short (:home_participant_short_name details)}
              :awayParticipants {:name (:away_participant_name details) 
                                 :short (:away_participant_short_name details)}
              :scoreAtTimeOfBet {:set {:home (:current_set_score_home details)
                                       :away (:current_set_score_away details)}
                                 :game {:home (:current_game_score_home details)
                                        :away (:current_game_score_away details)}
                                 :point {:home 0 :away 0}}}]
    (if has-final-score 
      (assoc base :finalScore {:set {:home set_score_home
                                     :away set_score_away}
                               :game {:home game_score_home
                                      :away game_score_away}
                               :point {:home 0 :away 0}})
      (assoc base :finalScore {:set {:home 0 :away 0} 
                               :game {:home 0 :away 0} 
                               :point {:home 0 :away 0}}))))

(defn serialize-cricket [details]
  (-> {}
      (into {:competition {:id (:competition_id details)
                           :name (:competition_name details)}})
      (into {:homeTeam {:name (:home_team_name details) 
                        :short (:home_team_short_name details)}})
      (into {:awayTeam {:name (:away_team_name details)
                        :short (:away_team_short_name details)}})
      (assoc :innings (:innings details))
      (assoc :delivery (:delivery details))
      (assoc :over (:over details))
      (into {:scoreAtTimeOfBet {:home (:current_score_home details) 
                                :away (:current_score_away details)}})
      (into {:dismissalsAtTimeOfBet {:home (:dismissals_home details) ;; FIXME current column tinyint(1) -> int
                                     :away (:dismissals_away details)}})
      (into {:finalScore {:home (:final_score_home details) 
                          :away (:final_score_away details)}})
      (into {:finalDismissals {:home (:final_dismissals_home details) ;; FIXME current column tinyint(1) -> int
                               :away (:final_dismissals_away details)}})))

(defn serialize-snooker [details]
  (-> {}
    (into {:competition {:id (:competition_id details)
                         :name (:competition_name details)}})
    (into {:homeTeam {:name (:home_team_name details) 
                      :short (:home_team_short_name details)}})
    (into {:awayTeam {:name (:away_team_name details)
                      :short (:away_team_short_name details)}})
    (into {:scoreAtTimeOfBet {:frame {:home (or (:current_frame_home details) 0) 
                                      :away (or (:current_frame_away details) 0)}
                              :score {:home 0 
                                      :away 0}}})
    (into {:finalScore {:frame {:home (:final_frame_home details) 
                                :away (:final_frame_away details)}
                        :score {:home 0 
                                :away 0}}})
    (assoc :totalFrame (:total_frame details))))

(defn serialize-darts [details] 
  (-> {}
    (into {:competition {:id (:competition_id details)
                         :name (:competition_name details)}})
    (into {:homeTeam {:name (:home_team_name details) 
                      :short (:home_team_short_name details)}})
    (into {:awayTeam {:name (:away_team_name details)
                      :short (:away_team_short_name details)}})
    (into {:bestOf {:set (> (:total_set details) 1)
                    :leg (and (= (:total_set details) 1) (> (:total_leg details) 1))
                    :numOfSet (:total_set details)
                    :numOfLeg (:total_leg details)}})
    (into {:scoreAtTimeOfBet {:set {:home (:current_set_home details)
                                    :away (:current_set_away details)}
                              :leg {:home (:current_leg_home details) 
                                    :away (:current_leg_away details)}
                              :point {:home 0 
                                      :away 0}}})
    (into {:finalScore {:set {:home (:final_set_home details)
                              :away (:final_set_away details)}
                        :leg {:home (:final_leg_home details) 
                              :away (:final_leg_away details)}
                        :point {:home 0 
                                :away 0}}})))

(defn serialize-rugby [details] 
  (-> {}
    (into {:competition {:id (:competition_id details)
                         :name (:competition_name details)}})
    (into {:homeTeam {:name (:home_team_name details) 
                      :short (:home_team_short_name details)}})
    (into {:awayTeam {:name (:away_team_name details)
                      :short (:away_team_short_name details)}})
    (into {:scoreAtTimeOfBet {:home (:current_home details)
                              :away (:current_away details)}})
    (into {:finalScore {:final (if (nil? (:final_home details)) 
                                  nil 
                                  {:home (:final_home details)
                                   :away (:final_away details)})
                        :extratime (if (nil? (:extratime_home details)) 
                                      nil
                                      {:home (:extratime_home details) 
                                       :away (:extratime_away details)})
                        :penalty (if (nil? (:penalty_home details)) 
                                    nil
                                    {:home (:penalty_home details) 
                                     :away (:penalty_home details)})}})))

(defn serialize-amfootball [details]
  (-> {}
    (into {:competition {:id (:competition_id details)
                         :name (:competition_name details)}})
    (into {:homeTeam {:name (:home_team_name details) 
                      :short (:home_team_short_name details)}})
    (into {:awayTeam {:name (:away_team_name details)
                      :short (:away_team_short_name details)}})
    (into {:scoreAtTimeOfBet {:home (:current_home details)
                              :away (:current_away details)}})
    (into {:finalScore {:final (if (nil? (:final_home details))
                                  nil
                                  {:home (:final_home details)
                                   :away (:final_away details)})}})))

(defn serialize-icehockey [details] 
  (-> {}
    (into {:competition {:id (:competition_id details)
                         :name (:competition_name details)}})
    (into {:homeTeam {:name (:home_team_name details) 
                      :short (:home_team_short_name details)}})
    (into {:awayTeam {:name (:away_team_name details)
                      :short (:away_team_short_name details)}})
    (into {:scoreAtTimeOfBet {:home (:current_home details)
                              :away (:current_away details)}})
    (into {:finalScore {:final (if (nil? (:final_home details))
                                  nil
                                  {:home (:final_home details)
                                   :away (:final_away details)})}})))

(defn get-formatted-cricketv2-score [score score-type]
  (try
    (let [type-index (if (= score-type :score) 0 1)]
      (if (nil? score)
        0
        (-> score
            (str/split #" & ")
            (reverse)
            (nth 0)
            (str/split #"/")
            (nth type-index)
            (Integer.))))
    (catch Exception e
      0)))

(defn serialize-cricketv2 [details] 
  (-> {}
    (into {:competition {:id (:competition_id details)
                         :name (:competition_name details)}})
    (into {:homeTeam {:name (:home_team_name details) 
                      :short (:home_team_short_name details)}})
    (into {:awayTeam {:name (:away_team_name details)
                      :short (:away_team_short_name details)}})
    (assoc :innings (:innings details))
    (assoc :delivery (:delivery details))
    (assoc :over (:over details))
    (into {:displayScoreAtTimeOfBet {:home (:current_score_home details) 
                                     :away (:current_score_away details)}})
    (into {:scoreAtTimeOfBet {:home (get-formatted-cricketv2-score (:current_score_home details) :score) 
                              :away (get-formatted-cricketv2-score (:current_score_away details) :score)}})
    (into {:dismissalsAtTimeOfBet {:home (get-formatted-cricketv2-score (:current_score_home details) :dismissal) 
                                   :away (get-formatted-cricketv2-score (:current_score_away details) :dismissal)}})
    (into {:finalScore {:home (:final_score_home details) 
                        :away (:final_score_away details)}})
    (into {:finalDismissals {:home (:final_dismissals_home details) 
                             :away (:final_dismissals_away details)}})
    (into {:dismissals {:home (get-formatted-cricketv2-score (:current_score_home details) :dismissal) ;; backward compatibility
                        :away (get-formatted-cricketv2-score (:current_score_away details) :dismissal)}})))

(defn serialize-sport [sport-id sport-details]
  (let [sport (static/get-key-by-id sport-id)]
    (cond 
      (= sport :football) (serialize-football sport-details)
      (= sport :tennis) (serialize-tennis sport-details)
      (= sport :cricket) (serialize-cricket sport-details)
      (= sport :snooker) (serialize-snooker sport-details)
      (= sport :darts) (serialize-darts sport-details)
      (= sport :rugby) (serialize-rugby sport-details)
      (= sport :amfootball) (serialize-amfootball sport-details)
      (= sport :icehockey) (serialize-icehockey sport-details)
      (= sport :cricketv2) (serialize-cricketv2 sport-details))))
  
(defn serialize-bet [bet]
  (let [{:keys [:id :sport_id :event_id :event_betradar_id :event_start
                :result :resolved_source :settled_at :odds :fractional
                :offer_id :outcome_id :extra_value :offer_name :outcome_name 
                :feed_odds :margin :z_variable :risk_category_id :sport_details 
                :status :display-status]} bet
        sport-details (first sport_details)
        event {:id event_id
               :betradarId event_betradar_id
               :startDate event_start
               :status status}
        sport (serialize-sport sport_id sport-details)]
    {:betId id
     :sportId sport_id
     :odds odds
     :oddsStr (str odds)
     :fractional fractional
     :offerId offer_id
     :offerName offer_name
     :outcomeId outcome_id
     :outcomeName outcome_name
     :extraValue extra_value
     :margin margin
     :displayStatus display-status
     :event (into event sport)}))

(defn ticket [ticket]
  (if (empty? ticket)
    {}
    (let [base {:id (:id ticket)
                :displayId (:display_id ticket)
                :userId (:user_id ticket)
                :createdAt (:created_at ticket)
                :toBeAcceptedAt (:to_be_accepted_at ticket) 
                :acceptedAt (:accepted_at ticket)  
                :rejectedAt (:rejected_at ticket)
                :rejectedReason (:rejection_reason ticket)
                :status (serialize-ticket-status (:to_be_accepted_at ticket) (:rejected_at ticket))
                :amount (:amount ticket)
                :result (serialize-ticket-result (:result ticket) (:settled_at ticket) (:payout ticket))
                :ip (:ip ticket)
                :betCount (:bet_count ticket)
                :resolvedBetCount (:resolved_bet_count ticket)
                :accumulator (serialize-ticket-accumulator ticket)
                :bets (map serialize-bet (:bets ticket))}
          single-bet? (= (:bet_count ticket) 1)]
      (if single-bet?
        (do 
          (let [single (first (:bets base))
                balance {:balancedOdds (:balanced_odds ticket)
                        :balancedFractional (:balanced_fractional ticket)
                        :balanceCost (:balance_cost ticket)
                        :multiplier (:multiplier ticket)}]         
            (assoc base :bets [(into single balance)])))
        base))))