(ns services.event
  (:require [clojure.data.json :as json]
            [ipc.reqrep :as reqrep]
            [db.pending-timer :as pt-model]))

(defn getforticket-payload [bets]
  (let [events 
          (map #(hash-map :sportId (:sportId %) 
                          :eventId (or (:eventId %) 
                                       (get-in bets [:event :id])))
               bets)]
    {:events events}))

(defn call-getforticket [payload] 
  (reqrep/send "offering" "event:getforticket" payload))

(defn merge-bet-event [bets events]
  (map #(into %1 %2) bets events))

(defn check-event-suspended [events]
  (let [suspended-events (filter #(not= (:suspended %) 0) events)]
    (if (pos? (count suspended-events))
      (throw
        (ex-info "Event suspended"
          {:code "EVENT_SUSPENDED" 
           :message "event suspended"})))))

(defn find-offer
  [offers id]
  (or (first (filter #(= (get % :id) id) offers)) {}))

(defn need-check-extra-value [outcome]
  (some? (:extraValue outcome)))

(defn find-outcome 
  [offer odds outcome-id extra-value]
  (let [outcomes (:outcomes offer [])
        found (->> (filter 
                      #(and (= odds (:odds %)) 
                            (= outcome-id (:outcomeId %))) 
                      outcomes)
                   (first))]
    (if (need-check-extra-value found)
      (if (= (:extraValue found) extra-value) 
        found
        nil)
      found)))

(defn find-matching-outcome [bet event]
  (let [{:keys [:odds :offerId :outcomeId :extraValue]} bet
         offers (get-in event [:details :offers])
         found-offer (find-offer offers offerId)]
    (find-outcome found-offer odds outcomeId extraValue)))

(defn odds-still-valid [bet event]
  (let [found-outcome (find-matching-outcome bet event)]
    (if (some? found-outcome) 
      (do
        (println "found outcome:" found-outcome)
        true)
      (do
        (println "odds not found:" bet)
        false))))

(defn check-odds-changed [bets events]
  (let [bet-event-pairs (map vector bets events)
        changed-cnt 
          (->> bet-event-pairs
               (map #(odds-still-valid (% 0) (% 1)))
               (filter #(false? %))
               (count))]
    (if (not= changed-cnt 0) 
      (throw 
        (ex-info "Odds changed"
          {:code "INVALID_ODDS"
           :message "odds changed"})))))

(defn round2
  "Round a double to the given precision (number of significant digits)"
  [precision d]
  (let [factor (Math/pow 10 precision)]
    (/ (Math/round (* d factor)) factor)))
  
(defn check-acca-odds [acca-odds bets]
  (let [calc-odds (round2 2 (reduce #(* %1 (:odds %2)) 1 bets))]
    (if (nil? acca-odds)
      true
      (if (not= acca-odds calc-odds)
        (throw 
          (ex-info "Invalid acca odds received"
            {:code "INVALID_ACCUMULATOR_ODDS"
             :message (str "acca-odds: " acca-odds ", calc-odds: " calc-odds)}))))))

(defn get-events [bets]
  (let [payload (getforticket-payload bets)
        {:keys [:success events]} (call-getforticket payload)]
    (if success
      events
      (throw 
        (ex-info "Unable to get events for ticket"
          {:code "UNEXPECTED_ERROR"})))))

(defn get-max-pending-timer-parameters [event]
  (let [{:keys [:sportId :id :competition]} event]
    (hash-map :sport-id sportId
              :event-id id
              :competition-id (:id competition))))
          
(defn get-pending-timer [events]
  (->> events
    (map get-max-pending-timer-parameters)
    (map pt-model/get-pending-timer)
    (map #(:delay %))
    (apply max)))

(defn get-events-status [event-ids]
  (let [payload {:eventIds event-ids}]
    (reqrep/send "offering" "event:geteventsstatus" payload)))

(defn get-valid-betradar-ids [betradar-ids limit offset]
  (let [payload {:sportId 0
                 :ids betradar-ids
                 :limit limit
                 :offset offset}
        resp (reqrep/send "offering" "event:findbybetradarids" payload)]
    (if (true? (:success resp))
      (let [events (:events resp)]
        (map #(:matchId %) events))
      [])))