(ns services.obs
  (:require [clojure.data.json :as json]
            [ipc.reqrep :as reqrep]
            [db.betyield :as by-model]))

(def median-limit-count 20)
(def obs-median-bet-min-count 10)
(def obs-bet-yield-initial-value 0.9)

(declare get-bet-yield get-median-stake get-boost getboost-payload call-obsboost)

(defn get-boost [ticket coord]
  (let [{:keys [:id :user_id :amount :bets]} ticket
        acca-bet? (> (count bets) 1)
        by-data (get-bet-yield user_id)
        median-stake (get-median-stake user_id median-limit-count)
        payload (getboost-payload ticket coord by-data median-stake)
        response (call-obsboost payload)]
    (-> response
        (assoc :bet-yield (:bet-yield by-data))
        (assoc :normal-bet-yield (:normal-bet-yield by-data)))))

(defn get-bet-yield [user-id]
  (let [{:keys [:total_bet_count
                :total_balanced_bet_count
                :normal_bet_yield
                :theoretical_bet_yield]} (by-model/get-user-raw-bet-yield user-id)
        data {:total-bet-count total_bet_count
              :total-balanced-bet-count total_balanced_bet_count}]
    (if (< total_bet_count obs-median-bet-min-count)
      (into data {:bet-yield obs-bet-yield-initial-value
                  :normal-bet-yield obs-bet-yield-initial-value})
      (into data {:bet-yield (max theoretical_bet_yield obs-bet-yield-initial-value)
                  :normal-bet-yield (max normal_bet_yield obs-bet-yield-initial-value)}))))

(defn get-median-stake [user-id limit-count]
  (let [stake-list (apply list (by-model/get-user-median-bet-stake user-id limit-count))
        list-cnt (count stake-list)]
    (cond 
      (zero? list-cnt) 
        0
      (even? (mod list-cnt 2))
        (let [odd-idx (- (/ list-cnt 2) 1)
              even-idx (/ list-cnt 2)]
          (/ (+ (:amount (nth stake-list odd-idx)) (:amount (nth stake-list even-idx))) 2))
      :else
        (:amount (nth stake-list (int (/ list-cnt 2))))
      )))

(defn create-event-name [bet]
  (let [tennis? (= (:sport_id bet) 2)
        sport-detail (nth (:sport_details bet) 0)]
    (if tennis? 
      (str (:home_participant_name sport-detail) " vs " (:away_participant_name sport-detail))
      (str (:home_team_name sport-detail) " vs " (:away_team_name sport-detail)))))

(defn create-acca-detail-per-bet [bet]
  {:sportId (:sport_id bet)
   :eventId (:event_id bet)
   :odds (:odds bet)
   :offerId (:offer_id bet)
   :outcomeId (:outcome_id bet)
   :extraValue (:extra_value bet)
   :eventName (create-event-name bet)})

(defn calculate-acc-margin [bets]
  (let [acca-bet? (> (count bets) 1)]
    (cond
      acca-bet?
        (reduce #(* %1 (with-precision 2 (/ (:odds %2) (:feed_odds %2)))) 1.0 bets)
      :else
        (let [single-bet (nth bets 0)]
          (with-precision 2 (/ (:odds single-bet) (:feed_odds single-bet)))))))

(defn getboost-payload [ticket coord by-data median-stake]
  (let [{:keys [:id :user_id :amount :bets]} ticket
        single-bet (nth bets 0)
        acca-bet? (> (count bets) 1)
        sport-id (if acca-bet? 0 (:sport_id single-bet))
        odds (:odds ticket)
        event-id (if acca-bet? 0 (:event_id single-bet))
        offer-id (if acca-bet? 0 (:offer_id single-bet))
        outcome-id (if acca-bet? 0 (:outcome_id single-bet))
        extra-value (if acca-bet? 0 (:extra_value single-bet))
        margin (calculate-acc-margin bets)
        event-name (if acca-bet? "Accumulator" (create-event-name single-bet))
        number-of-sets (if acca-bet? 0 (if (= sport-id 2) (get-in single-bet [:sport_details :number_of_sets]) nil))
        acca-detail (if acca-bet? (map #(create-acca-detail-per-bet %) bets) {})]
    (-> {} 
        (assoc :sportId sport-id)
        (assoc :ticketId id)
        (assoc :userId user_id)
        (assoc :betSize amount)
        (assoc :odds odds)
        (assoc :medianBetSize median-stake)
        (assoc :eventId event-id)
        (assoc :offerId offer-id)
        (assoc :outcomeId outcome-id)
        (assoc :extraValue extra-value)
        (assoc :margin margin)
        (assoc :coord coord)
        (assoc :eventName event-name)
        (assoc :numberOfSets number-of-sets)
        (assoc :accumulatorDetail acca-detail)
        (assoc :betYield (:bet-yield by-data))
        (assoc :normalBetYield (:normal-bet-yield by-data))
        (assoc :prevBetCount (:total-bet-count by-data))
        (assoc :prevBalancedBetCount (:total-balanced-bet-count by-data)))))

(defn call-obsboost [payload]
  (reqrep/send "obs" "oddsbalance:create" payload))
