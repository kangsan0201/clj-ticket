(ns services.ticket
  (:require [clojure.data.json :as json]
            [ipc.reqrep :as reqrep]
            [services.event :as event-service]
            [db.static :refer [sport-ids get-key-by-id bet-results event-status acca-display-status]]
            [db.ticket :as ticket-model]))
  
(defn get-display-id []
  (let [len 8
        alphabet (range 65 91)
        digit (range 48 58)
        character-set (concat alphabet digit)]
    (apply str (take len (repeatedly #(char (nth character-set (rand-int (count character-set)))))))))

(defn create-ticket-item [data accept-delay]
  (let [acca? (some? (get-in data [:accumulator :odds]))
        first-bet (-> data :bets first)]
    {:display_id (get-display-id)
     :user_id (:userId data)
     :ip (:ip data)
     :bet_count (count (:bets data))
     :amount (:amount data)
     :odds (if acca? 
              (get-in data [:accumulator :odds])
              (:odds first-bet))
     :fractional (if acca? 
                    (get-in data [:accumulator :fractional])
                    (:fractional first-bet))
     :wait_timer_sec accept-delay}))

(defn convert-timestamp [d]
  (-> d
    (clojure.string/replace #"T" " ")
    (clojure.string/replace #"Z" "")))

(defn gen-bet-item [bet event]
  (let [outcome (event-service/find-matching-outcome bet event)]
    {:sport_id (:sportId bet)
     :event_id (:eventId bet)
     :event_betradar_id (:matchId event)
     :event_start (convert-timestamp (:startDate event))
     :odds (:odds bet)
     :fractional (:fractional bet)
     :offer_id (:offerId bet)
     :outcome_id (:outcomeId bet)
     :extra_value (:extraValue bet)
     :offer_name (:offerName bet)
     :outcome_name (:outcomeName bet)
     :feed_odds (:feedOdds outcome)
     :margin (:margin outcome)
     :z_variable 0
     :risk_category_id (:riskCategory event)}))

(defn set-default-score [event key-vector]
  (let [score (get-in event key-vector)]
    (if (nil? score) 0 score)))

(defn gen-sport-specific [event]
  (let [sport-id (:sportId event)
        sport (get-key-by-id sport-id)]
    (cond 
      (= sport :football)
        {:event_current_score_home (set-default-score event [:details :score :home])
         :event_current_score_away (set-default-score event [:details :score :away])
         :matchtime_min (get-in event [:details :time :minutes])
         :matchtime_sec (get-in event [:details :time :seconds])
         :matchtime_extra_min (get-in event [:details :time :extra :minutes])
         :matchtime_extra_sec (get-in event [:details :time :extra :seconds])}
      (= sport :tennis)
        {:number_of_sets (:numberOfSets event)
         :home_participant_name (get-in event [:homeParticipants :name])
         :home_participant_short_name (get-in event [:homeParticipants :short])
         :away_participant_name (get-in event [:awayParticipants :name])
         :away_participant_short_name (get-in event [:awayParticipants :short])
         :current_set_score_home (set-default-score event [:details :score :set :home])
         :current_set_score_away (set-default-score event [:details :score :set :away])
         :current_game_score_home (set-default-score event [:details :score :game :home])
         :current_game_score_away (set-default-score event [:details :score :game :away])}
      (= sport :cricket) 
        {} ;; NO MORE SUPPORT CRICKET
      (= sport :snooker)
        {:total_frame (:totalFrame event)
         :current_frame_home (set-default-score event [:details :score :frame :home])
         :current_frame_away (set-default-score event [:details :score :frame :away])
         :current_score_home (set-default-score event [:details :score :score :home])
         :current_score_away (set-default-score event [:details :score :score :away])}
      (= sport :darts)
        {:total_set (get-in event [:bestOf :numOfSet])
         :total_leg (get-in event [:bestOf :numOfLeg])
         :current_set_home (set-default-score event [:details :score :set :home])
         :current_set_away (set-default-score event [:details :score :set :away])
         :current_leg_home (set-default-score event [:details :score :leg :home])
         :current_leg_away (set-default-score event [:details :score :leg :away])
         :current_point_home (set-default-score event [:details :score :point :home])
         :current_point_away (set-default-score event [:details :score :point :away])}
      (= sport :rugby)
        {:current_home (set-default-score event [:details :score :current :home])
         :current_away (set-default-score event [:details :score :current :away])}
      (= sport :amfootball)
        {:current_home (set-default-score event [:details :score :current :home])
         :current_away (set-default-score event [:details :score :current :away])}
      (= sport :icehockey)
        {:current_home (set-default-score event [:details :score :current :home])
         :current_away (set-default-score event [:details :score :current :away])}
      (= sport :cricketv2)
        {:innings (set-default-score event [:details :innings])
         :delivery (set-default-score event [:details :delivery])
         :over (set-default-score event [:details :over])})))

(defn dissoc-team-if-tennis [common sport-id]
  (let [sport (get-key-by-id sport-id)]
    (if (= sport :tennis) 
      (-> common
          (dissoc :home_team_name)
          (dissoc :home_team_short_name)
          (dissoc :away_team_name)
          (dissoc :away_team_short_name))
      common)))

(defn gen-sport-item [event]
  (let [sport-id (:sportId event)
        common {:event_current_state (get-in event [:details :state] nil)
                :home_team_name (get-in event [:homeTeam :name])
                :home_team_short_name (get-in event [:homeTeam :short])
                :away_team_name (get-in event [:awayTeam :name])
                :away_team_short_name (get-in event [:awayTeam :short])
                :competition_id (get-in event [:competition :id])
                :competition_name (get-in event [:competition :name])}]
    (-> common
        (into (gen-sport-specific event))
        (dissoc-team-if-tennis sport-id))))

(defn create-bet-sport-pairs 
  "Return [[{bet} {sport}] [{bet} {sport}] [{bet} {sport}]]"
  [data events]
  (let [bets (:bets data)
        bet-event-pairs (map vector bets events)
        bet-items (map #(gen-bet-item (% 0) (% 1)) bet-event-pairs)
        sport-items (map #(gen-sport-item (% 1)) bet-event-pairs)]
    (map vector bet-items sport-items)))

(defn create-ticket 
  "Create records in tickets bets sport-bets tables and return ticket-id"
  [data events accept-delay]
  (let [ticket (create-ticket-item data accept-delay)
        bet-sport-pairs (create-bet-sport-pairs data events)]
    (ticket-model/create-ticket-bet-sport ticket bet-sport-pairs)))

(defn get-ticket-common [options]
  (ticket-model/select-ticket options))

(defn get-accept-wait-ticket [ticket-id]
  (let [options {:id ticket-id 
                 :accepted_at nil 
                 :rejected_at nil}]
    (get-ticket-common options)))

(defn get-accepted-ticket [ticket-id]
  (let [options {:id ticket-id
                  :to_be_accepted_at [not= nil]
                  :accepted_at [not= nil]
                  :rejected_at nil}]
    (get-ticket-common options)))

(defn accept-n-get-ticket [ticket boost]
  (let [ticket-id (:id ticket)
        fields {:bet_yield (:bet-yield boost)
                :normal_bet_yield (:normal-bet-yield boost)}
        fields-added 
          (if (true? (:balanced boost))
            (into fields {:balanced_odds (:balancedOdds boost)
                          :balanced_fractional (:balancedFractional boost)
                          :balance_cost (:oddsBalanceCost boost)
                          :multiplier (:multiplier boost)}) 
            fields)]
    (do
      (let [update-cnt (ticket-model/update-fields-to-accept ticket-id fields-added)]
        (if (not= update-cnt 1)
          (throw 
            (ex-info "Update ticket accepted failed"
              {:code "ACCEPT_TICKET_FAIL"}))
          (let [accepted-ticket (get-accepted-ticket ticket-id)]
            (if (nil? (:id accepted-ticket))
              (throw
                (ex-info "Ticket not found"
                  {:code "TICKET_NOT_FOUND"}))
              accepted-ticket)))))))

(defn reject-ticket [ticket-id reason]
  "Return true as success otherwise false"
  (pos? (ticket-model/update-ticket-rejected ticket-id reason)))

(defn get-rejected-ticket [ticket-id]
  (let [options {:id ticket-id
                 :accepted_at nil
                 :rejected_at [not= nil]
                 :rejection_reason [not= nil]}]
    (get-ticket-common options)))

(defn get-event-display-status [bet-result event-status event-suspended]
  (let [{:keys [:won :lost :halfWon :halfLost :push :canceled]} bet-results]
    (if (some? bet-result)
      (cond
        (= bet-result won) (:won acca-display-status)
        (= bet-result lost) (:lost acca-display-status)
        (= bet-result push) (:push acca-display-status)
        (= bet-result canceled) (:canceledwon acca-display-status))
      (if (not= event-status (:finished event-status))
        (if (pos? event-suspended) 
          (:suspended acca-display-status)
          (cond
            (= event-status (:live event-status)) (:live acca-display-status)
            (= event-status (:upcoming event-status)) (:upcoming acca-display-status)))
        nil))))
  
(defn update-ticket-details [ticket status-map]
  (let [bets (:bets ticket)
        event-ids (map #(:event_id %) bets)
        event-statuss (:status (event-service/get-events-status event-ids))] ; resp = {:success t/f :status []}
    (->> bets
         (map #(assoc % :status (-> %
                                    (:event_id)
                                    (status-map)
                                    (:eventStatus))))
         (map #(assoc % :display-status (get-event-display-status
                                          (:result %)
                                          (:eventStatus (status-map (:event_id %)))
                                          (:eventSuspended (status-map (:event_id %))))))
         (map #(assoc % :sport_id (if (= (:sport_id %) (:cricket sport-ids))
                                    (:cricketv2 sport-ids)
                                    (:sport_id %))))
         (assoc ticket :bets))))

(defn update-cricket-sport-id-cricketv2 [ticket]
  (let [{:keys [:cricket :cricketv2]} sport-ids
        bets (:bets ticket)]
    (-> bets
        (map #(if (= (:sport_id %) cricket) 
                (assoc % :sport_id cricketv2)
                %)
        (assoc ticket :bets)))))

(defn get-event-ids [tickets]
  (let [bet-list (mapcat #(:bets %) tickets)]
    (map #(:event_id %) bet-list)))
  
(defn list-normal [user-id active limit offset]
  (let [rows (ticket-model/select-ticket-ids user-id active limit offset)
        tickets (doall (map #(get-accepted-ticket (:id %)) rows))
        unique-event-ids (-> (get-event-ids tickets)
                             (set) 
                             ((partial apply vector)))
        status-vector (:status (event-service/get-events-status unique-event-ids))
        status-map (reduce #(assoc %1 (:eventId %2) %2) {} status-vector)]
    ; (println "===> tickets" tickets)
    ; (println "===> unique-event-ids" unique-event-ids)
    ; (println "===> status-map" status-map)
    (map #(update-ticket-details % status-map) tickets)))

(defn list-group-by 
  "Return settled ticket list grouped by event-id"
  [user-id active limit offset]
  (let [rows (ticket-model/select-betradar-ids user-id)
        betradar-ids (map #(:event_betradar_id %) rows)
        valid-betradar-ids (event-service/get-valid-betradar-ids betradar-ids limit offset)
        tickets (ticket-model/select-tickets-group-by user-id valid-betradar-ids)]
    
    (println "betradar-ids: " betradar-ids)
    (println "valid-betradar-ids: " valid-betradar-ids)
    (println "tickets: " tickets)
    []))

(defn get-list
  "Return vector of tickets or empty vector if error occurs"
  [user-id active limit offset group-by]
  (try
    (if (= group-by "event")
      (list-group-by user-id active limit offset)
      (list-normal user-id active limit offset))
    (catch Exception e
      (.printStackTrace e))
    (finally
      [])))
