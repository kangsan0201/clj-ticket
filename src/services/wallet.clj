(ns services.wallet
  (:require [clojure.data.json :as json]
            [ipc.reqrep :as reqrep]
            [services.obs :as obs-service]))

(defn loss-limit-payload [user-id limit-amount limit-period]
  (let [payload {:userId user-id}]
    (into payload
      (if (and (not (empty? limit-amount)) 
               (not (empty? limit-period)))
            {:profitLossOnly true :period (or limit-period "")}))))

(defn call-wallet-loss-status [payload]
  (reqrep/send "wallet" "wallet:losslimit" payload))

(defn loss-limit-ok? [user-id stake loss-amount loss-period]
  (let [payload (loss-limit-payload user-id loss-amount loss-period)
        wallet-loss-status (:limit (call-wallet-loss-status payload) {})
        limit-amount (or (:amount wallet-loss-status 0) loss-amount 0)
        limit-period (or (:period wallet-loss-status nil) loss-period)
        potential-loss (+ (:loss wallet-loss-status 0) stake)]
    (cond
      (zero? limit-amount) 
        true
      (> potential-loss limit-amount)
        (throw
          (ex-info "User loss limit reached"
            {:code "LOSS_LIMIT_REACHED" 
             :message "Loss limit reached"
             :data {:amount limit-amount
                    :period limit-period
                    :potentialLoss potential-loss}}))
      :else
        true)))

(defn create-payload-acca-bet-details [bets]
  (map #(hash-map :sportId (:sport_id %)
                  :offerId (:offer_id %)
                  :eventName (obs-service/create-event-name %)) bets))
              
(defn create-payload-single-bet-details [bets]
  (let [first-bet (nth bets 0)]
    {:sportId (:sport_id first-bet)
     :offerId (:offer_id first-bet)
     :eventName (obs-service/create-event-name first-bet)}))

(defn pay-ticket-payload [ticket]
  (let [{:keys [:id :display_id :user_id :amount :bets]} ticket
        acca-bet? (> (count bets) 1)
        payload {:userId user_id
                 :ticketId id
                 :ticketDisplayId display_id
                 :amount amount
                 :accumulator acca-bet?}]
    (if acca-bet?
      (assoc payload :bets (create-payload-acca-bet-details bets))
      (into payload (create-payload-single-bet-details bets)))))

(defn call-pay-ticket [payload]
  (reqrep/send "wallet" "wallet:payticket" payload))

(defn pay-ticket [ticket]
  (let [payload (pay-ticket-payload ticket)
        response (call-pay-ticket payload)]
    (if (not (true? (:success response)))
      (throw 
        (ex-info "Wallet payticket failed"
          {:code "INSUFFICIENT_FUNDS"}))
      true)))
