(ns services.risk
  (:require [clojure.data.json :as json]
            [ipc.reqrep :as reqrep]
            [services.event :as event-service]))

(defn accept-reject-payload-item [bet event]
  (let [outcome (event-service/find-matching-outcome bet event)
        {:keys [:sportId :odds :offerId :outcomeId :extraValue]} bet
        {:keys [:feedOdds :margin]} outcome
        {:keys [:id :status :riskCategory]} event
        item {}]
    (-> item
      (assoc :givenOdds odds)
      (assoc :offerId offerId)
      (assoc :outcomeId outcomeId)
      (assoc :extraValue extraValue)
      (assoc :sportId sportId)
      (assoc :eventId id)
      (assoc :eventStatus status)
      (assoc :riskCategoryId riskCategory)
      (assoc :zVariable 0)
      (assoc :feedOdds feedOdds)
      (assoc :averageOdds 0)
      (assoc :margin margin))))

(defn call-acceptreject [payload]
  (reqrep/send "risk" "risk:acceptreject" payload))

(defn call-risk-entry [payload]
  (reqrep/send "risk" "risk:createriskentries" payload))

(defn accept-reject-payload [user-id stake bets events]
  (let [payload {:userId user-id :stake stake}
        items (->> (map vector bets events)
                   (map #(accept-reject-payload-item (% 0) (% 1))))]
    (assoc payload :bets items)))

(defn event-status [events event-id]
  (let [event (first (filter #(= (:id %) event-id) events))]
    (:status event)))

(defn risk-entry-payload [ticket events]
  (let [bets (:bets ticket)
        payload {:userId (:user_id ticket)
                  :stake (:amount ticket)
                  :ticketId (:id ticket)}
        inner-bets (map #(-> {}
                            (assoc :sportId (:sport_id %))
                            (assoc :eventId (:event_id %))
                            (assoc :odds (:odds %))
                            (assoc :offerId (:offer_id %))
                            (assoc :outcomeId (:outcome_id %))
                            (assoc :extraValue (:extra_value %))
                            (assoc :eventStatus (event-status events (:event_id %))) ;; TODO
                            (assoc :riskCategoryId (:risk_category_id %))
                            (assoc :givenOdds (:odds %))
                            (assoc :average 0)
                            (assoc :margin (:margin %))
                            (assoc :zVariable (:z_variable %))) bets)]
    (-> payload
        (assoc :bets inner-bets))))

(defn accept-reject [user-id stake bets events]
  (let [payload (accept-reject-payload user-id stake bets events)
        resp (call-acceptreject payload)
        succ (:success resp)
        accepted (:accepted resp)]
    (do (println "risk response" resp))
    (cond
      (true? accepted)
        true
      (or (false? accepted) (nil? accepted))
        (throw
          (ex-info "Exceed maxstake"
            {:code "EXCEED_MAXSTAKE"
             :data {:maxStake (:maxAllowedStake resp 0)}}))
      :else 
        (throw
          (ex-info "Risk verification failed"
            {:code "RISK_VERIFICATION_FAILED"})))))

(defn risk-entry [ticket events]
  (let [payload (risk-entry-payload ticket events)]
    (try
      (future (call-risk-entry payload))
      (catch Exception e
        (do
          (.printStackTrace e))))))