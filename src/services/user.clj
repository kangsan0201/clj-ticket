(ns services.user
  (:require [clojure.data.json :as json]
            [ipc.reqrep :as reqrep]
            [services.wallet :as wallet-service]
            [error :refer [throw-error]]))

(defn get-user-bet-status [user-id]
  (let [data {:userId user-id}]
    (reqrep/send "user" "user:betstatus" data)))

(defn check-user-status [status]
  (let [{frozen :frozen timeout-until :onTimeOutUntil} status]
    (cond
      (not= frozen 0)
        (throw
          (ex-info "User is frozen"
            {:code "USER_IS_FROZEN"
             :message "user is frozen"}))
      (not (nil? timeout-until))
        (throw
          (ex-info "User timeout"
            {:code "TIMEOUT"
             :message "user is unavailable until"
             :data {:onTimeOutUntil timeout-until}}))
      :else
        true)))

(defn verify-user [user-id stake]
  (let [user-bet-status (get-user-bet-status user-id)
        user-bet-status-ok (check-user-status user-bet-status)]
    (if (true? user-bet-status-ok)
      (let [amount (get-in user-bet-status [:limit :amount] nil)
            period (get-in user-bet-status [:limit :period] nil)]
        (wallet-service/loss-limit-ok? user-id stake amount period))
      user-bet-status-ok)))

(defn check-user-odds-validation
  "Return true as odds check required otherwise false"
  [user-id]
  (let [{:keys [:success :tags]} (reqrep/send "user" "user:gettags" {:userId user-id})
        tag-codes (map #(:code %) tags)
        tag-code-cnt (count tag-codes)]
    (if (= success true)
      (cond
        (zero? tag-code-cnt) false
        (and (= tag-code-cnt 1) (= (nth tag-codes 0) "VIP")) false
        :else true)
      true)))
