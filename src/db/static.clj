(ns db.static)

(def sport-ids {:football   1
                :tennis     2
                :cricket    3
                :snooker    4
                :darts      5
                :rugby      6
                :amfootball 7
                :icehockey  8
                :cricketv2  10})

(defn get-key-by-id [id]
  (first (keep #(when (= (val %) id) (key %)) sport-ids)))

(def rejection-reason {:by-user   1
                       :by-system 2})

(def bet-results {:won        1
                  :lost       2
                  :halfWon    3
                  :halfLost   4
                  :push       5
                  :canceled   6})

(def event-status {:upcoming        0
                   :live            1
                   :finished        2
                   :canceled        3
                   :paused          4
                   :abandoned       5
                   :about-to-start  6})

(def acca-display-status {:upcoming  'Upcoming'
                          :live      'Live'
                          :suspended 'Suspended'
                          :won       'Won'
                          :lost      'Lost'
                          :push      'Push'
                          :canceled 'Void'})