(ns db.ticket
  (:use [korma.core]
        [korma.db]
        [db.models :as models]
        [db.static :as static]))

(defn create-ticket-bet-sport [ticket bet-sport-pairs]
  (transaction
    (let [ticket-res (insert models/tickets 
                        (values ticket))
          ticket-id (:generated_key ticket-res)]
      (doseq [bs bet-sport-pairs]
        (let [bet-res (insert models/bets 
                        (values (assoc (bs 0) :ticket_id ticket-id)))
              bet-id (:generated_key bet-res)
              sport-entity (->> (bs 0)
                                (:sport_id)
                                (static/get-key-by-id)
                                (get models/sport-entity-map))]
          (println "sport-entity" sport-entity)
          (insert sport-entity
            (values (assoc (bs 1) :bet_id bet-id)))))
      ticket-id)))

(defn select-ticket-bet [ticket-id options]
  (let [query-options (assoc options :id ticket-id)]
    (select models/tickets
      (where query-options)
      (with models/bets
        (fields (:fields models/bets))
        (fields [:bets.id :bet_id])
        (fields [:bets.result :bet_result])
        (fields [:bets.resolved_source :bet_resolved_source])
        (fields [:bets.settled_at :bet_settled_at])))))

(defn select-sport [sport-id bet-id]
  (let [sport (static/get-key-by-id sport-id)
        sport-entity (sport models/sport-entity-map)]
    (select sport-entity
      (where {:bet_id bet-id}))))

(defn select-ticket-bet-sport [ticket-id options]
  (let [ticket-bet (first (select-ticket-bet ticket-id options))
        sports (doall (map #(select-sport (:sport_id %) (:bet_id %))
                           (:bets ticket-bet)))]
    (->> (:bets ticket-bet)
         ((partial map #(assoc %2 :sport_details %1) sports))
         (assoc ticket-bet :bets))))

(defn update-fields-to-accept [ticket-id fields]
  (-> (update* "tickets")
      (set-fields fields)
      (set-fields {:to_be_accepted_at (sqlfn now)})
      (set-fields {:accepted_at (sqlfn now)})
      (where {:id ticket-id
              :rejected_at nil
              :rejection_reason nil})
      (update)))

(defn update-ticket-rejected
  "Return updated number of rows, it should be 1 as it is success otherwise 0"
  [ticket-id reason]
  (-> (update* "tickets")
      (set-fields {:rejected_at (sqlfn now)
                   :rejection_reason reason})
      (where {:id ticket-id
              :accepted_at nil
              :rejected_at nil
              :rejection_reason nil})
      (update)))
  
(defn select-ticket-ids
  "Return ticket ids"
  [user-id active _limit _offset]
  (-> (select* "tickets")
        (fields :id)
        (where {:user_id user-id
                :to_be_accepted_at [not= nil]
                :accepted_at [not= nil]
                :rejected_at nil
                :settled_at (if active nil [not= nil])})
        (limit _limit)
        (offset _offset)
        (order :created_at :DESC)
        (select)))
  
(defn select-betradar-ids
  "Return distinct event_betradar_id of settled tickets of user"
  [user-id]
  (-> (select* "tickets")
      (join "bets" (= :bets.ticket_id :id))
      (fields :bets.event_betradar_id)
      (modifier "DISTINCT")
      (where {:user_id user-id
              :to_be_accepted_at [not= nil]
              :accepted_at [not= nil]
              :rejected_at nil
              :settled_at [not= nil]})
      (select)))

(defn add-table-name-prefix [table-name columns]
  (map #(str table-name "." (name %)) columns))

(defn add-columns
  ([query columns]
    (reduce #(fields %1 %2) query columns))
  ([query columns table-name]
    (let [table-columns (add-table-name-prefix table-name columns)
          alias-vector (map vector columns table-columns)]
      (reduce #(fields %1 %2) query table-columns))))

(defn select-tickets-group-by [user-id betradar-ids]
  (println "betradar-ids" betradar-ids)
  (-> (select* "tickets")
      (join :inner "bets" (= :bets.ticket_id :id))
      (add-columns models/tickets-columns)
      (add-columns models/bets-columns "bets")
      (join :left "football_bets" (= :football_bets.bet_id :bets.id))
      (add-columns models/football-columns "football_bets")
      (join :left "tennis_bets" (= :tennis_bets.bet_id :bets.id))
      (add-columns models/tennis-columns "tennis_bets")
      (where {:user_id user-id
              :id 105097
              :to_be_accepted_at [not= nil]
              :accepted_at [not= nil]
              :rejected_at nil
              :settled_at [not= nil]})
              ; :bets.event_betradar_id [in betradar-ids]})
      (select)))

(defn select-ticket [options]
  (let [tickets
          (-> (select* "tickets")
              (add-columns models/tickets-columns)
              (where options)
              (select))]
    (if (empty? tickets)
      {}
      (let [ticket (nth tickets 0)
            ticket-id (:id ticket)
            bet-sport-pairs
              (-> (select* "bets")
                  (join :left "football_bets" (= :football_bets.bet_id :bets.id))
                  (join :left "tennis_bets" (= :tennis_bets.bet_id :bets.id))
                  (join :left "cricket_bets" (= :cricket_bets.bet_id :bets.id))
                  (join :left "snooker_bets" (= :snooker_bets.bet_id :bets.id))
                  (join :left "darts_bets" (= :darts_bets.bet_id :bets.id))
                  (join :left "rugby_bets" (= :rugby_bets.bet_id :bets.id))
                  (join :left "amfootball_bets" (= :amfootball_bets.bet_id :bets.id))
                  (join :left "icehockey_bets" (= :icehockey_bets.bet_id :bets.id))
                  (join :left "cricketv2_bets" (= :cricketv2_bets.bet_id :bets.id))
                  (add-columns models/bets-columns "bets")
                  (add-columns models/football-columns "football_bets")
                  (add-columns models/tennis-columns "tennis_bets")
                  (add-columns models/cricket-columns "cricket_bets")
                  (add-columns models/snooker-columns "snooker_bets")
                  (add-columns models/darts-columns "darts_bets")
                  (add-columns models/rugby-columns "rugby_bets")
                  (add-columns models/amfootball-columns "amfootball_bets")
                  (add-columns models/icehockey-columns "icehockey_bets")
                  (add-columns models/cricketv2-columns "cricketv2_bets")
                  (where {:ticket_id ticket-id})
                  (select))]
      (assoc ticket :bets bet-sport-pairs)))))
