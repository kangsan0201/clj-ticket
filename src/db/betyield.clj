(ns db.betyield
  (:use [korma.core]
        [korma.db]
        [db.models :as models]))

(def sql 
  "SELECT
    st.settled_stake,
    st.profit,
    (st.profit / st.settled_stake) AS normal_bet_yield,
    bt.total_balanced_bet_count,
    bt.sum_of_balanced_odds_unsettled_stake,
    bt.sum_of_balanced_odds_multiplied_unsettled_stake,
    ((st.profit + bt.sum_of_balanced_odds_multiplied_unsettled_stake) / (st.settled_stake + bt.sum_of_balanced_odds_unsettled_stake)) AS theoretical_bet_yield,
    tt.total_bet_count
  FROM
  (
    SELECT
      COALESCE(SUM(CASE WHEN result IN (1, 2, 3, 4) THEN amount ELSE 0 END), 0) AS settled_stake,
      COALESCE(SUM(CASE WHEN result = 1 THEN (CASE WHEN balanced_odds IS NOT NULL THEN balanced_odds * amount ELSE amount * odds END) ELSE 0 END), 0) AS profit
    FROM kwiff_ticket.tickets
    WHERE
      user_id = ?
      AND accepted_at IS NOT NULL
      AND rejected_at IS NULL
      AND result IS NOT NULL
      AND resolved_source IS NOT NULL
      AND settled_at IS NOT NULL
  ) AS st,

  (
    SELECT
      COUNT(*) AS total_balanced_bet_count,
      SUM(CASE WHEN settled_at IS NULL AND balanced_odds IS NOT NULL THEN amount ELSE 0 END) AS sum_of_balanced_odds_unsettled_stake,
      SUM(CASE WHEN settled_at IS NULL THEN (CASE WHEN balanced_odds IS NOT NULL THEN balanced_odds / odds * amount ELSE 0 END) ELSE 0 END) AS sum_of_balanced_odds_multiplied_unsettled_stake
    FROM kwiff_ticket.tickets
    WHERE
      user_id = ?
      AND balanced_odds IS NOT NULL
      AND accepted_at IS NOT NULL
      AND rejected_at IS NULL
  ) AS bt,

  (
    SELECT
      COUNT(*) AS total_bet_count
    FROM kwiff_ticket.tickets
    WHERE
      user_id = ?
      AND accepted_at IS NOT NULL
      AND rejected_at IS NULL
  ) AS tt")


(defn get-user-raw-bet-yield [user-id]
  (let [sql-vec [sql [user-id user-id user-id]]]
    (do
      (let [rows (exec-raw sql-vec :results)]
        (first rows)))))

(defn get-user-median-bet-stake [user-id limit-count]
  (-> (select* "tickets")
      (fields :amount)
      (where {:user_id user-id
              :accepted_at [not= nil]
              :rejected_at [= nil]})
      (order :id :desc)
      (limit (or limit-count 20))
      (select)))