(ns db.pending-timer
  (:use [korma.core]
        [korma.db]
        [db.models :as models]))

(defn get-pending-timer [item]
  (let [{:keys [:sport-id :event-id :competition-id]} item
        sport-id-all 0
        timers (select models/pending-timers
                (where (or (and (= :event_id event-id) (= :sport_id sport-id))
                           (and (= :competition_id competition-id) (= :sport_id sport-id) (= :event_id nil))
                           (and (= :sport_id sport-id) (= :competition_id nil) (= :event_id nil))
                           (and (= :sport_id sport-id-all) (= :competition_id nil) (= :event_id nil)))))]
   
   (let [event-timer (first (filter #(and (pos? (:sport_id %)) 
                                          (some? (:event_id %))) timers))
         comp-timer (first (filter #(and (pos? (:sport_id %)) 
                                         (some? (:competition_id %)) 
                                         (nil? (:event_id %))) timers))
         sport-timer (first (filter #(and (pos? (:sport_id %)) 
                                           (nil? (:event_id %)) 
                                           (nil? (:competition_id %))) timers))
         dflt-timer (first (filter #(zero? (:sport_id %)) timers))
         max-timer (first (filter #(some? %) [event-timer comp-timer sport-timer dflt-timer]))
         {:keys [:sport_id :event_id :competition_id :delay]} max-timer]
      {:sport-id sport_id
      :event-id event_id
      :competition-id competition_id
      :delay delay})))


