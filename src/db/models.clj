(ns db.models
  (:use [korma.db]
        [korma.core]
        [settings :refer [db-conf]]))

(def db-conn (defdb conn 
  (mysql {:db (:dbname db-conf)
               :user (:user db-conf)
               :password (:password db-conf)
               :host (:host db-conf)
               :port (:port db-conf 3306)
               :delimiters ""})))

(declare tickets bets football-bets tennis-bets cricket-bets snooker-bets darts-bets rugby-bets amfootball-bets icehockey-bets cricketv2-bets)
    
(defn convert-entity-fields [tn fields]
  (let [prefix-table-name (fn [field]
        (->> field
            (name)
            ((partial format "%s.%s" tn))
            (keyword)))]
    (->> fields
      (map #(prefix-table-name %))
      (vec))))

;; TEMP COLUMNS DEFINITION
(def tickets-columns
  [:id :display_id :result :resolved_source :settled_at 
   :user_id :ip :resolved_bet_count :bet_count 
   :amount :odds :fractional :balanced_odds :balanced_fractional 
   :balance_cost :multiplier :payout :bet_yield :normal_bet_yield 
   :wait_timer_sec :to_be_accepted_at :accepted_at :rejected_at :rejection_reason 
   :created_at :updated_at])

(def bets-columns
  [:sport_id :event_id :event_betradar_id :event_start
   :result :resolved_source :settled_at :odds :fractional
   :offer_id :outcome_id :extra_value :offer_name :outcome_name 
   :feed_odds :margin :z_variable :risk_category_id])

(def football-columns 
  [:event_current_state 
   :competition_id :competition_name 
   :home_team_name :home_team_short_name 
   :away_team_name :away_team_short_name 
   :event_current_score_home :event_current_score_away
   :event_half_score_home :event_half_score_away 
   :event_final_score_home :event_final_score_away 
   :event_et_score_home
   :event_et_score_away
   :event_pen_score_home :event_pen_score_away 
   :matchtime_min :matchtime_sec 
   :matchtime_extra_min :matchtime_extra_sec])

(def tennis-columns
  [:event_current_state
   :competition_id :competition_name
   :number_of_sets 
   :home_participant_name :home_participant_short_name
   :away_participant_name :away_participant_short_name 
   :current_set_score_home :current_set_score_away 
   :current_game_score_home :current_game_score_away 
   :current_point_home :current_point_away
   :set_score_home :set_score_away 
   :game_score_home :game_score_away 
   :point_home :point_away])

(def cricket-columns 
  [:bet_id :event_current_state
   :competition_id :competition_name
   :home_team_name :home_team_short_name 
   :away_team_name :away_team_short_name
   :innings :delivery :over
   :current_score_home :current_score_away
   :final_score_home :final_score_away
   :dismissals_home :dismissals_away
   :final_dismissals_home :final_dismissals_away])

(def snooker-columns 
  [:bet_id :event_current_state
   :competition_id :competition_name
   :home_team_name :home_team_short_name 
   :away_team_name :away_team_short_name
   :total_frame
   :current_frame_home :current_frame_away
   :current_score_home :current_score_away
   :final_frame_home :final_frame_away
   :final_score_home :final_score_away])

(def darts-columns 
  [:bet_id :event_current_state
   :competition_id :competition_name
   :home_team_name :home_team_short_name 
   :away_team_name :away_team_short_name
   :total_set :total_leg
   :current_set_home :current_set_away
   :current_leg_home :current_leg_away
   :current_point_home :current_point_away
   :final_set_home :final_set_away
   :final_leg_home :final_leg_away
   :final_point_home :final_point_away])

(def rugby-columns 
  [:bet_id :event_current_state
   :competition_id :competition_name
   :home_team_name :home_team_short_name 
   :away_team_name :away_team_short_name
   :current_home :current_away
   :final_home :final_away
   :extratime_home :extratime_away 
   :penalty_home :penalty_away])

(def amfootball-columns 
  [:bet_id :event_current_state
   :competition_id :competition_name
   :home_team_name :home_team_short_name 
   :away_team_name :away_team_short_name
   :current_home :current_away 
   :final_home :final_away])

(def icehockey-columns 
  [:bet_id :event_current_state
   :competition_id :competition_name
   :home_team_name :home_team_short_name 
   :away_team_name :away_team_short_name
   :current_home :current_away 
   :final_home :final_away 
   :overtime_home :overtime_away 
   :penalty_home :penalty_away])

(def cricketv2-columns 
  [:bet_id :event_current_state
   :competition_id :competition_name
   :home_team_name :home_team_short_name 
   :away_team_name :away_team_short_name
   :current_score_home :current_score_away 
   :final_score_home :final_score_away
   :innings :delivery :over
   :dismissals_home :dismissals_away
   :final_dismissals_home :final_dismissals_away])
;; ==============


(defentity tickets
  (pk :id)
  (table :tickets)
  (database db-conn)
  (has-many bets {:fk :ticket_id})
  (entity-fields :id :display_id :result :resolved_source :settled_at 
    :user_id :ip :resolved_bet_count :bet_count 
    :amount :odds :fractional :balanced_odds :balanced_fractional 
    :balance_cost :multiplier :payout :bet_yield :normal_bet_yield 
    :wait_timer_sec :to_be_accepted_at :accepted_at :rejected_at :rejection_reason 
    :created_at :updated_at))

(defentity bets
  (pk :id)
  (table :bets)
  (database db-conn)
  (belongs-to tickets {:fk :ticket_id})
  (entity-fields :ticket_id :sport_id :event_id :event_betradar_id :event_start
    :result :resolved_source :settled_at :odds :fractional
    :offer_id :outcome_id :extra_value :offer_name :outcome_name 
    :feed_odds :margin :z_variable :risk_category_id))

(defentity football-bets
  (pk :bet_id)
  (table :football_bets)
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields
    :event_current_state 
    :competition_id :competition_name 
    :home_team_name :home_team_short_name 
    :away_team_name :away_team_short_name 
    :event_current_score_home :event_current_score_away
    :event_half_score_home :event_half_score_away 
    :event_final_score_home :event_final_score_away 
    :event_et_score_home
    :event_et_score_away
    :event_pen_score_home :event_pen_score_away 
    :matchtime_min :matchtime_sec 
    :matchtime_extra_min :matchtime_extra_sec))

(defentity tennis-bets 
  (pk :bet_id)
  (table :tennis_bets)
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields 
    :event_current_state
    :competition_id :competition_name
    :number_of_sets 
    :home_participant_name :home_participant_short_name
    :away_participant_name :away_participant_short_name 
    :current_set_score_home :current_set_score_away 
    :current_game_score_home :current_game_score_away 
    :current_point_home :current_point_away
    :set_score_home :set_score_away 
    :game_score_home :game_score_away 
    :point_home :point_away))

(defentity cricket-bets
  (pk :bet_id)
  (table :cricket_bets) 
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields 
    :bet_id :event_current_state
    :competition_id :competition_name
    :home_team_name :home_team_short_name 
    :away_team_name :away_team_short_name
    :innings :delivery :over
    :current_score_home :current_score_away
    :final_score_home :final_score_away
    :dismissals_home :dismissals_away
    :final_dismissals_home :final_dismissals_away))

(defentity snooker-bets 
  (pk :bet_id)
  (table :snooker_bets)
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields 
    :bet_id :event_current_state
    :competition_id :competition_name
    :home_team_name :home_team_short_name 
    :away_team_name :away_team_short_name
    :total_frame
    :current_frame_home :current_frame_away
    :current_score_home :current_score_away
    :final_frame_home :final_frame_away
    :final_score_home :final_score_away))

(defentity darts-bets 
  (pk :bet_id)
  (table :darts_bets)
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields 
    :bet_id :event_current_state
    :competition_id :competition_name
    :home_team_name :home_team_short_name 
    :away_team_name :away_team_short_name
    :total_set :total_leg
    :current_set_home :current_set_away
    :current_leg_home :current_leg_away
    :current_point_home :current_point_away
    :final_set_home :final_set_away
    :final_leg_home :final_leg_away
    :final_point_home :final_point_away))

(defentity rugby-bets 
  (pk :bet_id)
  (table :rugby_bets) 
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields 
    :bet_id :event_current_state
    :competition_id :competition_name
    :home_team_name :home_team_short_name 
    :away_team_name :away_team_short_name
    :current_home :current_away
    :final_home :final_away
    :extratime_home :extratime_away 
    :penalty_home :penalty_away))

(defentity amfootball-bets 
  (pk :bet_id)
  (table :amfootball_bets)
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields 
    :bet_id :event_current_state
    :competition_id :competition_name
    :home_team_name :home_team_short_name 
    :away_team_name :away_team_short_name
    :current_home :current_away 
    :final_home :final_away))

(defentity icehockey-bets
  (pk :bet_id)
  (table :icehockey_bets)
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields 
    :bet_id :event_current_state
    :competition_id :competition_name
    :home_team_name :home_team_short_name 
    :away_team_name :away_team_short_name
    :current_home :current_away 
    :final_home :final_away 
    :overtime_home :overtime_away 
    :penalty_home :penalty_away))
  
(defentity cricketv2-bets 
  (pk :bet_id)
  (table :cricketv2_bets)
  (database db-conn)
  (belongs-to bets {:fk :bet_id})
  (entity-fields 
    :bet_id :event_current_state
    :competition_id :competition_name
    :home_team_name :home_team_short_name 
    :away_team_name :away_team_short_name
    :current_score_home :current_score_away 
    :final_score_home :final_score_away
    :innings :delivery :over
    :dismissals_home :dismissals_away
    :final_dismissals_home :final_dismissals_away))
  
(defentity pending-timers
  (pk :id)
  (table :ticket_timer_pending)
  (database db-conn)
  (entity-fields :sport_id :event_id :competition_id :delay))
  
(def sport-entity-map {:football football-bets
                       :tennis tennis-bets
                       :cricket cricket-bets
                       :snooker snooker-bets
                       :darts darts-bets
                       :rugby rugby-bets
                       :amfootball amfootball-bets
                       :icehockey icehockey-bets
                       :cricketv2 cricketv2-bets})