(ns error
  (:require [clojure.data.json :as json]))

(defn assoc-property [m k p]
  (if (empty? p) m (assoc m k p)))

(defn create-err [code message data]
  (let [err {:code code}]
    (-> err
        (assoc-property :message message)
        (assoc-property :data data))))

(defn throw-error [fail]
  (let [{:keys [:code :message :data]} fail
        err (create-err code message data)]
    (println "[ERROR]" err)
    (throw (ex-info "throw error" err))))